#Source: http://containertutorials.com/docker-compose/flask-mongo-compose.html
from flask import Flask, request, render_template, redirect, url_for, flash, session
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)

import os
from pymongo import MongoClient
from flask_bootstrap import Bootstrap

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length

app = Flask(__name__)

SECRET_KEY = "yeah, not actually a secret"
DEBUG = True
Bootstrap(app)
#There were issues in forming the wtforms in the html files without the bootstrap.
#Source:
#Creating a User Login System Using Python, Flask and MongoDB, https://www.youtube.com/watch?v=vVx1737auSE&t=930s

app.config.from_object(__name__)

client = MongoClient(
    os.environ['DB_PORT_27017_TCP_ADDR'],
    27017)
db = client.tododb

login_manager = LoginManager()

login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

login_manager.setup_app(app)

USER_LIST =[]
PW_LIST = []

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"


@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

class SignUpForm(FlaskForm):
    email = StringField('email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])

class LogInForm(FlaskForm):
    username = StringField('username', validators=[InputRequired(), Length(min = 4, max = 9)])
    password = PasswordField('password', validators=[InputRequired(), Length(min = 4, max = 9)])
    remember = BooleanField('remember me')


class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active


    def is_active(self):
        return self.active

@app.route('/')
def index():
     return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LogInForm()
    if form.validate_on_submit():
        return '<h1>' + form.username.data + ' ' + form.password.data + '</h1>'
        # login_user(user)

        # flask.flash('logged in successfully!')
        # return flask.redirect(next or flask.url_for('index'));

    return render_template('login.html', form = form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignUpForm()
    user = request.args.get('username')


    if form.validate_on_submit():

        USERDATA = request.form.getlist("username")
        PWDATA = request.form.getlist("password")

        if USERDATA not in USER_LIST:

            for x in USERDATA:
                if str(x)!='':
                    USER_LIST.append(str(x))
                    app.logger.debug("USERS: " + str(USER_LIST))
            for x in PWDATA:
                if str(x)!='':
                    PW_LIST.append(str(x))
                    app.logger.debug("PASSWORDS: " + str(PW_LIST))

            for x in range(len(USER_LIST)):
                print("USERS: "+ USER_LIST[x])
                print("PASSWORDS: "+ PW_LIST[x])
                item_doc = {
                'username': USER_LIST[x],
                'password': PW_LIST[x]
                }
                db.tododb.insert_one(item_doc)

            _items = db.tododb.find() 
            items = [item for item in _items]
            app.logger.debug("Contents of db: " + str(items))

            print("USERS: "+ USER_LIST[x])
            print("PASSWORDS: "+ PW_LIST[x])

            return 'Welcome new User!'

        else:
            return 'User already exists'
    return render_template('signup.html', form = form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return 'You are logged out!'


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)