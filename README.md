# Project 7: Authentication and user interface 
Email:sahlberg@uoregon.edu

## What is in this repository
This is a first draft of a sample of flask-login with MongoDB. This repository is incomplete, and a heads-up that when starting the software, it takes a little while for each website to load.

Once completed, the repository is meant to feature these functions:

- POST **/api/register**

Registers a new user. On success a status code 201 is returned. The body of the response contains a JSON object with the newly added user. A `Location` header contains the URI of the new user. On failure status code 400 (bad request) is returned. Note: The password is hashed before it is stored in the database. Once hashed, the original password is discarded. The database should have three fields: id (unique index), username and password for storing the credentials.

- GET **/api/token**

Returns a token. This request must be authenticated using a HTTP Basic Authentication (see password.py for example). On success a JSON object is returned with a field `token` set to the authentication token for the user and a field `duration` set to the (approximate) number of seconds the token is valid. On failure status code 401 (unauthorized) is returned.

- GET **/RESOURCE**

Return a protected <resource>. This request must be authenticated using token-based authentication only (see testToken.py). HTTP password-based (basic) authentication is not allowed. On success a JSON object with data for the authenticated user is returned. On failure status code 401 (unauthorized) is returned.

### Part 2: User interface

Furthermore, the goal of this part of the project is to create frontend/UI for Brevet app using Flask-WTF and Flask-Login introduced in 322 of Introduction to Software Engineering. The frontend/UI uses the authentication created, and three additional functionalities in the UI will be added: (a) remember me, (b) logout, and (c) CSRF protection. Note: You don’t have to maintain sessions.

